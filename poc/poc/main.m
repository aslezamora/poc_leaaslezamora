//
//  main.m
//  poc
//
//  Created by cpccqo143461 on 2/23/15.
//  Copyright (c) 2015 accenture. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
