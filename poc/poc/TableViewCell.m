//
//  TableViewCell.m
//  poc
//
//  Created by cpccqo143461 on 2/23/15.
//  Copyright (c) 2015 accenture. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell ()

@end

@implementation TableViewCell
@synthesize lblTitle, lblDescription, image;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //Title
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(7, 6, 235, 20)];
        lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
        
        //Description
        lblDescription = [[UILabel alloc]initWithFrame:CGRectMake(7, 30, 235, 40)];
        lblDescription.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0];
        lblDescription.lineBreakMode = NSLineBreakByWordWrapping;
        lblDescription.numberOfLines = 0;
        
        //Image
        image = [[UIImageView alloc]initWithFrame:CGRectMake(251, 6, 62, 58)];
        
        [self addSubview:lblTitle];
        [self addSubview:lblDescription];
        [self addSubview:image];
        [lblTitle release];
        [lblDescription release];
        [image release];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
