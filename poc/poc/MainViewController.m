//
//  MainViewController.m
//  poc
//
//  Created by cpccqo143461 on 2/23/15.
//  Copyright (c) 2015 accenture. All rights reserved.
//

#import "MainViewController.h"
#import "TableViewCell.h"

#define SYSTEM_VERSION_LESS_THAN(v)    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface MainViewController ()

@property (nonatomic, assign) UITableView *tbJson;
@property (nonatomic, assign) NSMutableArray *arrJson;
@property (nonatomic, assign) UIRefreshControl *refreshControl;
@property (nonatomic, assign) BOOL isDragging_msg, isDecliring_msg;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //TableView
    self.tbJson = [[UITableView alloc]initWithFrame:self.view.bounds];
    self.tbJson.dataSource = self;
    self.tbJson.delegate = self;
    
    [self.view addSubview:self.tbJson];
    
    //Getting the data from Json link
    [self getJsonData];
    
    //Pull to Refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(updateDatabase:) forControlEvents:UIControlEventValueChanged];
    [self.tbJson addSubview:self.refreshControl];
}

-(void)getJsonData{
    NSError *error;
    NSString *strJsonURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://dl.dropboxusercontent.com/u/746330/facts.json"] encoding:NSISOLatin1StringEncoding error:&error];
    
    NSData *response = [strJsonURL dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
    self.navigationItem.title = [dictionary objectForKey:@"title"];
    
    self.arrJson = [[[NSMutableArray alloc]init] retain];
    for (id item in [dictionary objectForKey:@"rows"]) {
        if ([item objectForKey:@"title"] != [NSNull null])
            [self.arrJson addObject:item];
    }
}

#pragma mark - Full to Refresh
- (void)updateDatabase:(id)sender{
    //Update Data
    [self getJsonData];
    [self performSelector:@selector(updateTable:) withObject:nil afterDelay:1];
}

- (void)updateTable:(id)sender
{
    
    [self.tbJson reloadData];
    
    [self.refreshControl endRefreshing];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrJson.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblDescription = [[UILabel alloc]initWithFrame:CGRectMake(7, 30, 235, 40)];
    lblDescription.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0];
    lblDescription.backgroundColor = [UIColor yellowColor];
    lblDescription.numberOfLines = 0;
    if ([[self.arrJson valueForKey:@"description"] objectAtIndex:indexPath.row] != [NSNull null])
        lblDescription.text = [[self.arrJson valueForKey:@"description"] objectAtIndex:indexPath.row];
    else
        lblDescription.text = @"";
    [lblDescription sizeToFit];
    float height = lblDescription.frame.size.height + 40;
    if (height < 70)
        height = 70;
    
    return height;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomCell";
    
    // Custom Cell
    TableViewCell *cell = (TableViewCell *)[self.tbJson dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
    }
    //Set Title
    cell.lblTitle.text = [[self.arrJson valueForKey:@"title"] objectAtIndex:indexPath.row];
    
    //Set Description
    if ([[self.arrJson valueForKey:@"description"] objectAtIndex:indexPath.row] != [NSNull null])
        cell.lblDescription.text = [[self.arrJson valueForKey:@"description"] objectAtIndex:indexPath.row];
    else
        cell.lblDescription.text = @"";

    //Change frame height based on description text    
    CGRect frame = CGRectMake(cell.lblDescription.frame.origin.x, cell.lblDescription.frame.origin.y, cell.lblDescription.frame.size.width, [self getTextHeight:cell.lblDescription.text atFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0]]);
    cell.lblDescription.frame = frame;
    
    
    //Set Image
    [cell.image setImage:[UIImage imageNamed:@"placeholder.png"]];
    //Lazy loading
    if ([[self.arrJson valueForKey:@"imageHref"] objectAtIndex:indexPath.row] != [NSNull null]){
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[self.arrJson valueForKey:@"imageHref"] objectAtIndex:indexPath.row]]];
            UIImage *image = [UIImage imageWithData:data]; dispatch_async(dispatch_get_main_queue(), ^{
                if(image) [cell.image setImage:image];
                cell.image.contentMode = UIViewContentModeScaleAspectFill;
            });
        });
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.navigationItem.title = [[self.arrJson valueForKey:@"title"] objectAtIndex:indexPath.row];
}

- (CGFloat) getTextHeight:(NSString*)str atFont:(UIFont*)font
{
    CGSize size = CGSizeMake(280,300);
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")){
        CGSize textSize = [str sizeWithFont:font constrainedToSize:size];
        return textSize.height;
    }else{
        CGRect textRect = [str
                           boundingRectWithSize:size
                           options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:@{NSFontAttributeName:font}
                           context:nil];
        return textRect.size.height;
    }
}

-(void)dealloc{
    [self.tbJson release];
    [self.arrJson release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
