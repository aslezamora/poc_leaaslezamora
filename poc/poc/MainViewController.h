//
//  MainViewController.h
//  poc
//
//  Created by cpccqo143461 on 2/23/15.
//  Copyright (c) 2015 accenture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
